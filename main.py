import telebot
import json

from telebot import types
from bot.config import TOKEN

##Обходим блокировку через прокси
##P.S эти прокси уже не работают.Можно использовать другие,но лучше использовать VPN
# from telebot import apihelper
#
# apihelper.proxy = {
#     'http': 'http://13.82.16.76:3128',
#     'https': 'http://13.82.16.76:3128',
# }
bot = telebot.TeleBot(TOKEN)

@bot.message_handler(commands=['start'])
def start(message):
    send_mess = f"<b>{message.from_user.first_name} {message.from_user.last_name}</b>, Вас приветствует тестовый бот." \
                f"\nНиже приведён список доступных команд:" \
                f"\n/more - Возвращает картинку;" \
                f"\n/watch - Возвращает видео;" \
                f"\n/sourse - Возвращает ссылку на исходник." \
                f"\nСпасибо за уделённое внимание."
    bot.send_message(message.chat.id, send_mess, parse_mode='html')

@bot.message_handler(commands=['more'])
def more(message):
    x = '{"fileSizeBytes":37818,"url":"https://random.dog/2539bc07-5097-4409-a4fa-6f8e885c42ad.jpg"}'
    y = json.loads(x)
    send_mess = f"<b>{message.from_user.first_name} {message.from_user.last_name}</b>, вот ваша картинка:"
    bot.send_message(message.chat.id, send_mess, parse_mode='html')
    bot.send_photo(message.chat.id, y["url"])

@bot.message_handler(commands=['watch'])
def open_website(message):
	markup = types.InlineKeyboardMarkup()
	markup.add(types.InlineKeyboardButton("Ваше видио", url="https://youtu.be/5qap5aO4i9A"))
	bot.send_message(message.chat.id,
			"lofi hip hop radio - beats to relax/study to",
			parse_mode='html', reply_markup=markup)

@bot.message_handler(commands=['sourse'])
def open_website(message):
	markup = types.InlineKeyboardMarkup()
	markup.add(types.InlineKeyboardButton("Смотреть код", url="https://gitlab.com/GeorgyZhulimov/test-telegram-bot-androis_seegson.git"))
	bot.send_message(message.chat.id,
			"Для просмотра исходного кода нажмите кнопку 'Смотреть код'.",
			parse_mode='html', reply_markup=markup)

bot.polling(none_stop=True)